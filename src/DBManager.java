import java.sql.*;

public class DBManager {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:E:\\TJEE\\lab1.naLabie2\\db";
    public static final String QUERY = "select * from app.pracownicy";
    private static java.sql.Connection conn;


    public static boolean connect() throws SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
        return conn != null;
    }

    public static boolean disconnect() throws SQLException {
        if (conn == null) return false;
        else {
            conn.close();
            return true;
        }
    }

    public static String getData() throws SQLException {
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = "";
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");
        }
        wiersz = wiersz.concat("\r\n");
        while (rs.next()) {
            System.out.println("");
            for (int i = 1; i <= colCount; i++) {
                wiersz = wiersz.concat(rs.getString(i) + " \t| ");
            }
            wiersz = wiersz.concat("\r\n");
        }
        stat.close();
        return wiersz;
    }

    public static void insertData(String nazwisko, String imie, String id) throws SQLException {
        Statement stat = conn.createStatement();
        String polecenie =  "INSERT INTO pracownicy VALUES " + "('" +  nazwisko + "', '" + imie + "', '" + id + "')";
        stat.executeUpdate(polecenie);
        stat.close();
    }

    public static boolean isConnected(){
        return conn!=null;
    }

}

