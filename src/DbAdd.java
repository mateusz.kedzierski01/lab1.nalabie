import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

public class DbAdd extends JDialog {
    private JPanel contentPane2;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textNazwisko;
    private JTextField textImie;
    private JTextField textIdentyfikator;
    private DbOkno parent;

    public DbAdd(DbOkno parent) {
        this.parent = parent;
        setContentPane(contentPane2);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane2.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        String nazwisko, imie, id;
        nazwisko = textNazwisko.getText();
        imie = textImie.getText();
        id = textIdentyfikator.getText();

        if (DBManager.isConnected() && !nazwisko.isEmpty() && !imie.isEmpty() && !id.isEmpty()) {
            try {
                DBManager.insertData(nazwisko, imie, id);
                parent.log("Dodano do bazy!");
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
