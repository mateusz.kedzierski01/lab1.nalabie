import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbOkno extends JDialog {
    private JTextArea tPole;
    private JButton buttonConnect;
    private JButton buttonPrint;
    private JButton buttonDisconnect;
    private JPanel contentPane;
    private JButton buttonAdd;
    boolean connect = false;

    public DbOkno() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonConnect);
        buttonConnect.addActionListener(this::bConnectActionPerformed);
        buttonDisconnect.addActionListener(this::bDisconnectActionPerformed);
        buttonPrint.addActionListener(this::bPrintActionPerformed);
        buttonAdd.addActionListener(this::bAddActionPerformed);
        getRootPane().setPreferredSize(new Dimension(400, 200));
        setTitle("Baza pracowników");
    }

    public static void main(String[] args) {
        DbOkno dialog = new DbOkno();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void bConnectActionPerformed(ActionEvent evt) {

        try {
            connect = DBManager.connect();
        } catch (SQLException ex) {
            Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (connect) {
            tPole.setText("Połączono");
        } else {
            tPole.setText("Błąd! Nie połączono");
        }
    }

    private void bDisconnectActionPerformed(ActionEvent evt) {
        try {
            connect = DBManager.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (connect) {
            tPole.setText("Rozłączono");
        } else {
            tPole.setText("Nie rozłączono");
        }
    }

    private void bPrintActionPerformed(ActionEvent evt) {
        try {
            if (connect) {
                String out = DBManager.getData();
                tPole.removeAll();
                tPole.setText(out);
            } else {
                tPole.setText("Najpierw trzeba połączyć z bazą!");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbOkno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void bAddActionPerformed(ActionEvent evt) {
        DbAdd dialog = new DbAdd(this);
        dialog.pack();
        dialog.setVisible(true);
    }

    public void log(String s) {
        tPole.append("\n" + s);
    }
}
